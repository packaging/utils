#!/bin/ash
# shellcheck shell=dash

set -e

# shellcheck disable=SC3010
if [[ -n "${DEBUG}" ]]; then
    set -x
fi

cleanup_error() {
  docker rm -f "utils-builder-${CI_JOB_ID}"
  exit 1
}

for job in createrepo reprepro
do
    docker rm -f "utils-builder-${CI_JOB_ID}" || true
    DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker create --name "utils-builder-${CI_JOB_ID}" \
      -e CI_PROJECT_DIR=/build \
      -e CI_JOB_NAME="${CI_JOB_NAME}" \
      -e NAME="${NAME}" \
      -e DEBUG="${DEBUG}" \
      ubuntu:noble /build/.gitlab-ci/"${job}".sh
    docker cp "${CI_PROJECT_DIR}" "utils-builder-${CI_JOB_ID}":/build || cleanup_error
    docker start "utils-builder-${CI_JOB_ID}" || cleanup_error
    docker attach "utils-builder-${CI_JOB_ID}" || cleanup_error
    docker cp "utils-builder-${CI_JOB_ID}":/build /tmp/
    loop=0
    while docker inspect "utils-builder-${CI_JOB_ID}" >/dev/null 2>&1
    do
        if [ "${loop}" -gt 10 ]; then
            break
        fi
        loop="$((loop+1))"
        docker rm -f "utils-builder-${CI_JOB_ID}" || true
    done
    cp -rv /tmp/build/"${CI_JOB_NAME}"* "${CI_PROJECT_DIR}"/
done
