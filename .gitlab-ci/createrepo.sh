#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive

script="$(readlink -f "$0")"
url="${URL:-http://deb.debian.org/debian/pool/main/c/createrepo-c}"

apt-get update
apt-get -y install \
    ccache \
    curl \
    debian-keyring \
    devscripts \
    equivs \
    jq
export PATH="/usr/lib/ccache/bin/:$PATH"
export CCACHE_DIR="$CI_PROJECT_DIR/ccache"
export DEB_BUILD_OPTIONS=nocheck
mkdir -p "$CCACHE_DIR"
upstream="$(curl -s "https://sources.debian.org/api/src/createrepo-c/?suite=${UPSTREAM_SUITE:-bookworm}" | jq -re '.versions[].version')"
curl -LO "${url}/createrepo-c_${upstream}.dsc"
curl -LO "${url}/createrepo-c_${upstream%%-*}.orig.tar.gz"
curl -LO "${url}/createrepo-c_${upstream}.debian.tar.xz"
rm -rf "createrepo-c-${upstream%%-*}"
dpkg-source -x "createrepo-c_${upstream}.dsc"
mk-build-deps "createrepo-c-${upstream%%-*}/debian/control"
apt-get -y install "./createrepo-c-build-deps_${upstream}_all.deb"
rm -f "createrepo-c-build-deps_${upstream}_all.deb"
cd "createrepo-c-${upstream%%-*}"/
debuild \
    --preserve-envvar=CCACHE_DIR \
    --preserve-envvar=DEB_BUILD_OPTIONS \
    --prepend-path=/usr/lib/ccache \
    --no-lintian \
    -us -uc \
    -j"$(getconf _NPROCESSORS_ONLN)"
cd -
owner="$(stat -c %u "${script}")"
group="$(stat -c %g "${script}")"
install -d -o "${owner}" -g "${group}" "${CI_PROJECT_DIR:-${PWD}}/${CI_JOB_NAME}"
install -m 644 -v -o "${owner}" -g "${group}" "${PWD}"/*.deb "${CI_PROJECT_DIR:-${PWD}}/${CI_JOB_NAME}/"
git clean -fdx \
    -e ccache \
    -e ccache/** \
    -e "${CI_JOB_NAME}" \
    -e "${CI_JOB_NAME}"/** || true
