#!/bin/bash

set -e

apt-get -qq update
apt-get -qqy install "${CI_PROJECT_DIR}"/*/*amd64.deb
for codename in noble; do
    ARCHITECTURES="amd64 arm64" SCAN_DIR="${CI_PROJECT_DIR}"/"${codename}"-* "${CI_PROJECT_DIR}/deb.sh" "${codename}"
done
