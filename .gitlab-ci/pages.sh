#!/bin/bash

set -e

mkdir -p "${CI_PROJECT_DIR}"/public/noble
cp -rv "${CI_PROJECT_DIR}"/.repo/deb/gpg.key "${CI_PROJECT_DIR}"/public/
cp -rv "${CI_PROJECT_DIR}"/.repo/deb/noble/noble/{dists,pool} "${CI_PROJECT_DIR}/public/noble/"
