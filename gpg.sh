#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

repo_name="${REPO_NAME:-${1}}"
packager="${PACKAGER:-deb}"
keyring_name="${KEYRING_NAME:-${repo_name}-keyring}"
system_arch="$(dpkg --print-architecture)"
maintainer="${MAINTAINER:-Stefan Heitmüller <morph@morph027.de>}"

# import keys, create keyring package
mapfile -t signing_keys < <(printenv | grep -o --color=never "^SIGNING_KEY_[0-9]\{4\}_[_0-9]*" | sort)

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.37.1}"
    apt-get -qq update
    apt-get -qqy install curl
    curl -sfLo /tmp/nfpm.deb "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${system_arch}.deb"
    apt-get -qqy install /tmp/nfpm.deb
fi

keyring_files=""
fingerprints=()

for signing_key in "${signing_keys[@]}"
do
    # shellcheck disable=SC2206
    split=(${signing_key//_/ })
    id="${split[*]:2}"
    id="${id// /-}"
    keyring_version="${signing_key//SIGNING_KEY_/}"
    if [ -f "${!signing_key}" ]; then
        gpg --import "${!signing_key}" 2>&1 | tee /tmp/gpg.log
    else
        gpg --import <<<"${!signing_key}" 2>&1 | tee /tmp/gpg.log
    fi
    fingerprint="$(grep -o "key [0-9A-Z]*:" /tmp/gpg.log | grep -o "[0-9A-Z]*" | tail -n1)"
    fingerprints+=("${fingerprint}")
    gpg --export "${fingerprint}" >"${keyring_name}-${id}.gpg"
    keyring_files+="${keyring_name}-${id}.gpg "
done

case "${packager}" in
    "deb")
        arch="all"
        ;;
    "rpm")
        arch="noarch"
        ;;
esac

cat >/tmp/keyring.yaml <<EOF
name: ${keyring_name}
arch: ${arch}
version: ${keyring_version//_/-}
version_schema: none
maintainer: ${maintainer}
contents:
EOF
for keyring_file in ${keyring_files}
do
    {
        echo "  - src: ${keyring_file}"
        echo "    dst: /etc/apt/trusted.gpg.d/"
    }>>/tmp/keyring.yaml
done
nfpm package --config /tmp/keyring.yaml --packager "${packager}"

# export key for curl
test -f "${CI_PROJECT_DIR}/gpg.key" || gpg --export --armor "${fingerprints[@]}" >"${CI_PROJECT_DIR}/gpg.key"
