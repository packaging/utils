#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
repo_name="${REPO_NAME:-${1}}"
scan_dir="${SCAN_DIR:-${CI_PROJECT_DIR}}"
keyring_name="${KEYRING_NAME:-${repo_name}-keyring}"
releasever="${RELEASEVER:-${repo_name}}"
limit="${LIMIT:-0}"

# create keyring
env PACKAGER=rpm KEYRING_NAME="${keyring_name}" REPO_NAME="${repo_name}" "${script_dir}"/gpg.sh

mkdir -p "${CI_PROJECT_DIR}/build-dummy-dir-for-find-to-succeed"

# get fingerprints from gpg
mapfile -t fingerprints < <(gpg --list-packets <"${CI_PROJECT_DIR}/gpg.key" | grep --color=never -o 'keyid .*$' | cut -d' ' -f2 | sort -u)
echo "%_gpg_name ${fingerprints[-1]}" >~/.rpmmacros

# import rpm key
rpm --import "${CI_PROJECT_DIR}/gpg.key"

# add packages, sorted by version starting w/ highest version to lowest
mapfile -t packages < <(find "${scan_dir}" -type f -name "*.rpm" -not -path '*/.repo/*' | sort -rV)

declare -A package_count

# cleanup packages regarding limit
cleanup() {
    local key="${1}"
    current="${package_count[${key}]}"
    current=$((current+1))
    package_count["${key}"]="${current}"
    if [ "${limit}" -gt 1 ]; then
        if [ "${package_count[${key}]}" -gt "${limit}" ]; then
            return 0
        fi
    fi
    return 1
}

repos=()

for package in "${packages[@]}"; do
    package_name="$(rpm -qp --qf "%{NAME}\n" "${package}")"
    package_version="$(rpm -qp --qf "%{VERSION}\n" "${package}")"
    package_arch="$(rpm -qp --qf "%{ARCH}\n" "${package}")"
    if cleanup "${package_name}_${package_arch}"; then
        printf "\e[1;36m[%s %s] Package %s %s (%s) is affected by LIMIT=${LIMIT}/revocation list ... \e\033[0;38;5;160mRemoving\e[0m\n" "${releasever}" "${package_name}" "${package_version}" "${package_arch}"
        rm -f "${package}"
        filename="${package##*/}"
        find "${CI_PROJECT_DIR}/.repo/rpm" -type f -name "${filename}" -delete
        continue
    fi
    mkdir -p "${CI_PROJECT_DIR}/.repo/rpm/${releasever}/${package_arch}"
    printf "\e[1;36m[%s] Checking for package %s %s (%s) in current rpm repo cache ...\e[0m " "${releasever}" "${package_name}" "${package_version}" "${package_arch}"
    if [ -f "${CI_PROJECT_DIR}/.repo/rpm/${releasever}/${package_arch}/${package##*/}" ]; then
        printf "\e[0;32mOK\e[0m\n"
        continue
    fi
    printf "\e\033[0;38;5;164mAdding\e[0m\n"
    rpm --addsign "${package}"
    cp -v "${package}" "${CI_PROJECT_DIR}/.repo/rpm/${releasever}/${package_arch}/"
    if ! grep -q "${CI_PROJECT_DIR}/.repo/rpm/${releasever}" <<<"${repos[@]}"; then
        repos+=("${CI_PROJECT_DIR}/.repo/rpm/${releasever}")
    fi
done

# create repo
for repo in "${repos[@]}"
do
    createrepo_opts="--xz"
    if [ -f "${repo}"/repodata/repomd.xml ]; then
        createrepo_opts+=" --update"
    fi
    # shellcheck disable=SC2086
    createrepo_c ${createrepo_opts} "${repo}"
    # shellcheck disable=SC2086
    gpg --batch --yes --detach-sign --armor --local-user "${fingerprints[-1]}" "${repo}"/repodata/repomd.xml
done
if [ -d "${CI_PROJECT_DIR}/.repo" ]; then
    cp -v "${CI_PROJECT_DIR}/gpg.key" "${CI_PROJECT_DIR}/.repo/rpm/"
fi
