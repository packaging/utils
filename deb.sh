#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
repo_name="${REPO_NAME:-${1}}"
scan_dir="${SCAN_DIR:-${CI_PROJECT_DIR}}"
keyring_name="${KEYRING_NAME:-${repo_name}-keyring}"
origin="${ORIGIN:-${repo_name}}"
suite="${SUITE:-${repo_name}}"
label="${LABEL:-${repo_name}}"
codename="${CODENAME:-${repo_name}}"
component="${COMPONENT:-main}"
architectures="${ARCHITECTURES:-amd64}"
limit="${LIMIT:-0}"
basedir="${CI_PROJECT_DIR}/.repo/deb/${repo_name}/${codename}"
reprepro_basedir="reprepro -b ${basedir}"
reprepro="${reprepro_basedir} -C ${component}"

env PACKAGER=deb KEYRING_NAME="${keyring_name}" REPO_NAME="${repo_name}" "${script_dir}"/gpg.sh

# get fingerprints from gpg
mapfile -t fingerprints < <(gpg --list-packets <"${CI_PROJECT_DIR}/gpg.key" | grep --color=never -o 'keyid .*$' | cut -d' ' -f2 | sort -u)

# add repo config template if none exists
if [ ! -f "${basedir}/conf/distributions" ]; then
    mkdir -p "${basedir}/conf"
    {
        echo "Origin: ${origin}"
        echo "Suite: ${suite}"
        echo "Label: ${label}"
        echo "Codename: ${codename}"
        echo "Components: ${component}"
        echo "Architectures: ${architectures}"
        echo "SignWith: ${fingerprints[*]}"
        echo "Limit: ${limit}"
        echo ""
    } >>"${basedir}/conf/distributions"
fi

if ! grep -q "^Components:.*${component}" "${basedir}/conf/distributions"; then
    sed -i "s,^Components: \(.*\),Components: \1 ${component}, " "${basedir}/conf/distributions"
fi

if ! grep -q "^Architectures:.*${architectures}" "${basedir}/conf/distributions"; then
    sed -i "s,^Architectures: \(.*\),Architectures: \1 ${component}, " "${basedir}/conf/distributions"
fi

# export key for curl, configure reprepro (sign w/ multiple keys)
test -f "${CI_PROJECT_DIR}/.repo/gpg.key" || gpg --export --armor "${fingerprints[@]}" >"${CI_PROJECT_DIR}/.repo/gpg.key"
sed -i 's,##SIGNING_KEY_ID##,'"${fingerprints[*]}"',' "${basedir}/conf/distributions"
mkdir -p "${CI_PROJECT_DIR}/build-${codename}-dummy-dir-for-find-to-succeed"

# add packages, sorted by version starting w/ highest version to lowest
# shellcheck disable=SC2086
mapfile -t packages < <(find ${scan_dir} -type f -name "*.deb" -not -path '*/.repo/*' | sort -rV)

declare -A package_count

# cleanup packages regarding limit and revocation list
cleanup() {
    local name="${1}"
    local version="${2}"
    local arch="${3}"
    local key="${name}_${arch}"
    current="${package_count[${key}]}"
    current=$((current+1))
    package_count["${key}"]="${current}"
    if [ "${limit}" -ge 1 ]; then
        if [ "${package_count[${key}]}" -gt "${limit}" ]; then
            return 0
        fi
    fi
    if grep -q "^${name}=${version}$" "${CI_PROJECT_DIR}/.repo/revoked"; then
        return 0
    fi
    return 1
}


includedebs=()
declare -A removedebs

for package in "${packages[@]}"; do
    package_name="$(dpkg -f "${package}" Package)"
    package_version="$(dpkg -f "${package}" Version)"
    package_arch="$(dpkg -f "${package}" Architecture)"
    if cleanup "${package_name}" "${package_version}" "${package_arch}"; then
        printf "\e[1;36m[%s %s] Package %s %s (%s) is affected by LIMIT=${LIMIT}/revocation list ... \e\033[0;38;5;160mRemoving\e[0m\n" "${codename}" "${component}" "${package_name}" "${package_version}" "${package_arch}"
        rm -f "${package}"
        removedebs["${package_arch}"]+=" ${package_name}=${package_version}"
        continue
    fi
    printf "\e[1;36m[%s %s] Checking for package %s %s (%s) in current deb repo cache ...\e[0m " "${codename}" "${component}" "${package_name}" "${package_version}" "${package_arch}"
    case "${package_arch}" in
    "all")
        # shellcheck disable=SC2016
        filter='Package (=='"${package_name}"'), $Version (=='"${package_version}"')'
        ;;
    *)
        # shellcheck disable=SC2016
        filter='Package (=='"${package_name}"'), $Version (=='"${package_version}"'), $Architecture (=='"${package_arch}"')'
        ;;
    esac
    if [ -d "${basedir}/db" ]; then
        if $reprepro listfilter "${codename}" "${filter}" | grep -q '.*'; then
            printf "\e[0;32mOK\e[0m\n"
            continue
        fi
    fi
    if grep -q "${package##*/}" <<<"${includedebs[@]}"; then
        printf "\e[0;32mOK\e[0m\n"
        continue
    fi
    printf "\e\033[0;38;5;164mAdding\e[0m\n"
    includedebs+=("${package}")
done

if [ -n "${!removedebs[*]}" ]; then
    for arch in "${!removedebs[@]}"
    do
        # shellcheck disable=SC2086
        $reprepro \
            -A "${arch}" \
            -vvv \
            remove \
            "${codename}" \
            ${removedebs[${arch}]}
    done
fi

# shellcheck disable=SC2128
if [ -n "${includedebs}" ]; then
    $reprepro \
        -vvv \
        includedeb \
        "${codename}" \
        "${includedebs[@]}"
fi

if ! $reprepro_basedir -v checkpool fast |& tee /tmp/missing; then
    printf "\e[0;36mStarting repo cache cleanup ...\e[0m\n"
    mapfile -t missingfiles < <(grep "Missing file" /tmp/log | grep --color=never -o "/.*\.deb")
    for missingfile in "${missingfiles[@]}"; do
        missingfile="${missingfile##*/}"
        name="$(cut -d'_' -f 1 <<<"${missingfile}")"
        version="$(cut -d'_' -f 2 <<<"${missingfile}")"
        echo "cleanup missing file ${missingfile} from repo"
        $reprepro \
            -v \
            remove \
            "${codename}" \
            "${name}=${version}"
    done
fi

if [ -d "${CI_PROJECT_DIR}/.repo" ]; then
    cp -v "${CI_PROJECT_DIR}/gpg.key" "${CI_PROJECT_DIR}/.repo/deb/"
fi
