# Packaging namespace utils

Content:

* latest reprepro and createrepo-c
* `rpm.sh` and `deb.sh` to create repos

## Use Docker Image

You can use the pre-built docker image `registry.gitlab.com/packaging/utils:latest`

## Use Packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-utils.asc https://packaging.gitlab.io/utils/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/utils noble main" | sudo tee /etc/apt/sources.list.d/morph027-utils.list
```

## Scripts

Both repo scripts use `gpg.sh` to create a keyring package containing the public signing keys.

### Common parameters

* `SIGNING_KEY_<YYYY>_<MM>_<DD>`: private signing key
* `REPO_NAME`: name of the repository (will create folders and is default value to some other parameters)
* `SCAN_DIR`: directory to scan for packages to include in the repo
* `KEYRING_NAME`: keyring name (defaults to `REPO_NAME`)
 
### `deb.sh`

* `ORIGIN`: repo metadata - origin (defaults to `REPO_NAME`)
* `SUITE`: repo metadata - suite (defaults to `REPO_NAME`)
* `LABEL`: repo metadata - label (defaults to `REPO_NAME`)
* `CODENAME`: repo metadata - codename (defaults to `REPO_NAME`)
* `COMPONENT`: repo metadata - component (defaults to `main`)
* `ARCHITECTURES`: repo architectures to handle (defaults to `amd64`)
* `LIMIT`: how many versions to keep of the same package (defaults to `0`)

### `rpm.sh`

* `RELEASEVER`: repo release version (defaults to `REPO_NAME`)

## Extras/Hints/Notes

### repo signing key rotation

#### create new key

```bash
# Example
gpg2 --quick-generate-key "morph027 Automatic Signing Key ($(date +%Y)) <morph@morph027.de>" rsa4096 sign 5y
```
